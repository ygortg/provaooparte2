package Models;

public class Paciente extends Usuario{
	
	private String sintomas;
	private boolean clinicaMedica, pediatria;
	
	
	public Paciente(String nome, String sintomas) {
		super(nome);
		this.sintomas = sintomas;
	}
	
	public String getSintomas() {
		return sintomas;
	}
	public void setSintomas(String sintomas) {
		this.sintomas = sintomas;
	}
	public boolean isClinicaMedica() {
		return clinicaMedica;
	}
	public void setClinicaMedica(boolean clinicaMedica) {
		this.clinicaMedica = clinicaMedica;
	}
	public boolean isPediatria() {
		return pediatria;
	}
	public void setPediatria(boolean pediatria) {
		this.pediatria = pediatria;
	}
	
	

}