package View;

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Controlers.ControlerUsuario;
import Models.Paciente;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BuscarPaciente extends JInternalFrame {

	private JTextField txtNome;
	static ControlerUsuario umControladorUsuario;

	/**
	 * Create the frame.
	 */
	public BuscarPaciente() {
		
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblBusqueUmNome = new JLabel("Busque um Nome");
		
		lblBusqueUmNome.setBounds(12, 28, 169, 15);
		getContentPane().add(lblBusqueUmNome);
		
		JTextArea txtrNome = new JTextArea();
		txtrNome.setText("nome");
		txtrNome.setBounds(182, 28, 1, 15);
		getContentPane().add(txtrNome);
		
		txtNome = new JTextField();
		txtNome.setText("Nome");
		txtNome.setBounds(172, 26, 114, 19);
		getContentPane().add(txtNome);
		txtNome.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nomeBusca;
				
				nomeBusca = txtNome.getText();
			
				try{
					JOptionPane.showMessageDialog(null,"Nome: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeBusca).getNome()+
							"\nIdade: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeBusca).getIdade()+
							"\nCPF: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeBusca).getCpf()+
							"\nRG: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeBusca).getRg()+
							"\nSintomas: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeBusca).getSintomas()+
							"\nTelefone: "+TelaCadastroPaciente.umControladorUsuario.buscarPaciente(nomeBusca).getTelefone());
					
					
				}catch(NullPointerException b){
					JOptionPane.showMessageDialog(null, "Paciente não cadastrado");
				}
			}
		});
		btnBuscar.setActionCommand("Buscar");
		btnBuscar.setBounds(64, 217, 117, 25);
		getContentPane().add(btnBuscar);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnFechar.setBounds(240, 217, 117, 25);
		getContentPane().add(btnFechar);


	}

}
